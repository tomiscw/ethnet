﻿using System.Threading.Tasks;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Nethereum.HdWallet;

namespace Ethnet
{
    /// <summary>
    /// Wrapper around Nethereum API
    /// </summary>
    public class EthNetwork
    {
        const string MAINNET = "https://mainnet.infura.io";
        const string TESTNET = "https://rinkeby.infura.io";

        public static Web3 Account(string seed, string address)
        {
            var account = new Wallet(seed, null).GetAccount(address);
            return Network(account);

        }

        public static Web3 Account(string seed, string password, string address)
        {
            var account = new Wallet(seed, password).GetAccount(address);
            return Network(account);

        }

        public async static Task<decimal> BalanceAsync(string address)
        {
            var network = Network();
            var balance = await network.Eth.GetBalance.SendRequestAsync(address);
            return Web3.Convert.FromWei(balance.Value);
        }

        /// <summary>
        /// Resolves which Ethereum network to connect to, mainnet or Rinkeby,
        /// based on weather TestNet is set to true or false. It'll fallback to
        /// Rinkeby by default, in order to prevent accidental spending on the mainnet.
        /// Note that the Debug setting has no effect on which network is choosen.
        /// </summary>
        /// <returns>Mainnet (myetherapi.com), if false or Testnet (etherscan.io), if true</returns>
        public static Web3 Network()
        {
            switch (EthnetConfig.Config.CustomNet)
            {
                case "":
                case null:
                    switch (EthnetConfig.Config.TestNet)
                    {
                        default:
                        case true:
                            return new Web3(TESTNET);
                        case false:
                            return new Web3(MAINNET);
                    }

                default:
                    return new Web3(EthnetConfig.Config.CustomNet);
            }
        }

        /// <summary>
        /// Resolves which Ethereum network to connect to, mainnet or Rinkeby,
        /// based on weather TestNet is set to true or false. It'll fallback to
        /// Rinkeby by default, in order to prevent accidental spending on the mainnet.
        /// Note that the Debug setting has no effect on which network is choosen.
        /// </summary>
        /// <param name="account">HD Wallet</param>
        /// <returns>Mainnet (myetherapi.com), if false or Testnet (etherscan.io), if true</returns>
        public static Web3 Network(Account account)
        {
            switch (EthnetConfig.Config.CustomNet)
            {
                case "":
                case null:
                    switch (EthnetConfig.Config.TestNet)
                    {
                        default:
                        case true:
                            return new Web3(account, TESTNET);
                        case false:
                            return new Web3(account, MAINNET);
                    }

                default:
                    return new Web3(account, EthnetConfig.Config.CustomNet);
            }
        }
    }
}
