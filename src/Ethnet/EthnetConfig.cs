﻿using System.IO;
using Nett;

namespace Ethnet
{
    public class EthnetConfig
    {
        #region Settings
        public bool Debug { get; set; } = true;

        /// <summary>
        /// Connect to a test network
        /// </summary>
        public bool TestNet { get; set; } = true;

        /// <summary>
        /// CustomNet will always overwrite TestNet.
        /// </summary>
        public string CustomNet { get; set; } = "";
        #endregion

        const string CONFIG_FILE = "Ethnet.toml";

        public static EthnetConfig Config
        {
            get
            {
                EthnetConfig cfg;

                if (File.Exists(CONFIG_FILE))
                    cfg = Toml.ReadFile<EthnetConfig>(CONFIG_FILE);
                else
                    cfg = new EthnetConfig();

                return cfg;
            }
        }

        /// <summary>
        /// Generates the config file, if none exists.
        /// </summary>
        public static void GenerateFile()
        {
            var cfg = new EthnetConfig();

            if (!File.Exists(CONFIG_FILE))
                Toml.WriteFile(cfg, CONFIG_FILE);
        }
    }
}
