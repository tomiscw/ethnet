﻿namespace Ethnet
{
    internal class Customer
    {
        /// <summary>
        /// Name of the wallet
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ethereum address
        /// </summary>
        public string Account { get; set; }
    }
    public class EthnetDB
    {
    }
}
