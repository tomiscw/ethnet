﻿using System.Threading.Tasks;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Nethereum.Contracts;

namespace Bitoleon
{
    public class Bitoleon
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public static async Task<Contract> ContractAsync(Account account)
        {
            var web3 = new Web3(account);
            var totalSupply = int.Parse("500000000000000000000");
            var receipt = await web3.Eth.DeployContract.SendRequestAndWaitForReceiptAsync(BitoleonSpec.BITOLEON_ABI, BitoleonSpec.BITOLEON_BYTECODE);
            return web3.Eth.GetContract(BitoleonSpec.BITOLEON_ABI, receipt.ContractAddress);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="newAddress"></param>
        /// <param name="sendAddress"></param>
        /// <param name="amountToSend"></param>
        /// <returns></returns>
        public static async Task TransferAsync(Account account, string newAddress, string sendAddress, int amountToSend)
        {
            var contract = await ContractAsync(account);
            var transferFunc = contract.GetFunction("transfer");
            var balanceFunc = contract.GetFunction("balance");
            var gas = await transferFunc.EstimateGasAsync(newAddress, null, null, sendAddress, amountToSend);
            var receiptFirstAmountSend = await transferFunc.SendTransactionAndWaitForReceiptAsync(sendAddress, gas, null, null, newAddress, amountToSend);
            var balanceFirstAmountSend = await balanceFunc.CallAsync<int>(newAddress);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="account"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static async Task MintAsync(Account account, string address, int amount)
        {
            var contract = await ContractAsync(account);
            var mintFunc = contract.GetFunction("mint");

            await mintFunc.CallAsync<bool>(address, amount);
        }
    }
}
