![](imgs/bitoleonbanner.png)

Bitoleon /bɪtəʊliːˈɒn/, a play on Simoleon, is a ERC20-based token for use with [MyEthnet](https://github.com/tomiscw/ethnet) project, currently on Rinkeby.

- **Address:** [0x3597344afb2dc5f9707979f74b0309696d368174](https://rinkeby.etherscan.io/token/0x3597344afb2dc5f9707979f74b0309696d368174?a=0x341a3a994a150962f3e82b195873b736daeb4bb3)
- **Name:** Bitoleon
- **Symbol:** BLN
- **Decmials:** 18