![](imgs/ethnetbanner.png)

Ethnet is an experimental .NET Core-based Ethereum dApp using [Nethereum](https://nethereum.com/), currently in the prototyping stages. By default it connects to the Rinkeby testnet; however, it can connect to mainnet or a private network, such as Quorum, from a optional config file.

## MyEthnet

MyEthnet is the main front-end based on ASP.Net Core. The goal is to create an working online wallet and virtual economy with the Bitoleon /bɪtəʊliːˈɒn/ token.

## Support

Feel free to support the project.

![](imgs/ethereum-0x341.png) ![](imgs/monero-44xZ.png)